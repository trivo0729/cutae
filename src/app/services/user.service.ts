import { Injectable } from '@angular/core';
import { User, loginDetails } from '../modals/user.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private httpClient: HttpClient) { }
   /*UserLogin*/ 
   public LoginDetails : loginDetails = new loginDetails();
   UserLogin(UserName:string,Password:string,sParentID:number){
      return this.httpClient.post<loginDetails>("http://clickurtrip.net/Webservices/Authorize/service/loginHandler.asmx/Userlogin",
      {UserName,Password,sParentID},
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
       })
      });
  }


  setLogedIn(loginDetails:string)
  {
      localStorage.setItem('LoginDetails', loginDetails )
  }

  getLogedIn()
  {
    try  {
      this.LoginDetails= JSON.parse(localStorage.getItem('LoginDetails'));
      console.log(this.LoginDetails)
     return  this.LoginDetails;
    }
    catch(ex)
    {

    }
   
  }

}
