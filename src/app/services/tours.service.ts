import {Injectable  } from '@angular/core';
import { Tours, TourType, DateRange ,Slots, TicketType, arrLocation, Search_Params, resultTours, TourTypes} from '../modals/tours.modals';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Promise } from 'q';
import { Observable } from 'rxjs';
import { GlobalDefaultService, SerchParams_Tour } from './global-variable.service';
@Injectable({
  providedIn: 'root'
})
export class ToursService {
  arrTours :Tours;
  arrTourType:TourType;
  arrDates: DateRange;
  objGlobal:GlobalDefaultService;
  Parent:number =232;
  Search_Params:Search_Params = new Search_Params();
  
  constructor(private httpClient: HttpClient){}

/*Get All Tours*/ 
getLocation(Name:string): Observable<arrLocation[]>{
  return this.httpClient.post<arrLocation[]>("http://clickurtrip.net/Webservices/Activity/Handler/GetActivityHandler.asmx/GetLocations",
  {
   ParentID: this.Parent, Name
  },{ headers: new HttpHeaders({'Content-Type': 'application/json'})});
}

  /*Get All Tours*/ 
  getTours(): Observable<resultTours>{
    console.log(this.Search_Params)
      return this.httpClient.post<resultTours>("http://clickurtrip.net/Webservices/Activity/Handler/GetActivityHandler.asmx/SearchActivities",
      {LocationID: this.Search_Params.LocationID,Date: this.Search_Params.sDate,TourType: this.Search_Params.TourType},
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
       })
      });
  }
  
/*Get  Tour Type By Tour*/ 
 getTourtype(ID: Number,ActivityDate:string): Observable<TourType[]>{
    return this.httpClient.post<TourType[]>("http://clickurtrip.net/Webservices/Activity/Handler/GetActivityHandler.asmx/GetRateType",{ID,ActivityDate},{
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  } 
/*Get  All Tour Type*/ 
getAllTourtype(): Observable<TourTypes[]>{
  return this.httpClient.post<TourTypes[]>("http://clickurtrip.net/Webservices/Activity/Handler/GetActivityHandler.asmx/GetTourType",{ParentID:this.Parent},{
      headers: new HttpHeaders({'Content-Type': 'application/json'})
  });
}


  /*Avail Dates*/ 
  getDateRange({ Ratetype, ID, Date }: { Ratetype: string; ID: Number; Date: string; }): Observable<DateRange[]>{
    return this.httpClient.post<DateRange[]>( "http://clickurtrip.net/Webservices/Activity/Handler/GetActivityHandler.asmx/DateArray",
    {ID,Date,Ratetype},{headers: new HttpHeaders({'Content-Type': 'application/json'})})
  }

  /*Avail Slots*/
  getSlots({ Type, ID, Date }: { Type: string; ID: Number; Date: string; }): Observable<Slots[]>{
    return this.httpClient.post<Slots[]>( "http://clickurtrip.net/Webservices/Activity/Handler/GetActivityHandler.asmx/SlotByDate",
    {
      ID,Date,Type
    },
    {headers: new HttpHeaders({'Content-Type': 'application/json'})})
  }

  /*Slot  Rates*/
  getSlotRate({ Type, ID,SlotID, Date }: { Type: string; ID: Number;SlotID:Number; Date: string; }): Observable<TicketType[]>{
    return this.httpClient.post<TicketType[]>("http://clickurtrip.net/Webservices/Activity/Handler/GetActivityHandler.asmx/GetRates",
    {
      Type,ID,SlotID,Date
    },{headers: new HttpHeaders({'Content-Type': 'application/json'})})
  } 
}