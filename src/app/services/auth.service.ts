import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
// import {auth0} from '@angular/auth0'
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _idToken: string;
  private _accessToken: string;
  private _expiresAt: number;
  constructor(public router: Router) {
    this._idToken = '';
    this._accessToken = '';
    this._expiresAt = 0;
  }
  // get accessToken(): string {
  //   return this._accessToken;
  // }

  // get idToken(): string {
  //   return this._idToken;
  // }

  // public login(): void {
  //   this.auth0.authorize();
  // }
}
