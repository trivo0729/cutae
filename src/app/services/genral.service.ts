import { Injectable } from '@angular/core';
import { User } from '../modals/user.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Dbhelper } from '../modals/dbhelper.model';

@Injectable({
  providedIn: 'root'
})
export class GenralService {
 Parent:number=232;
  constructor(private httpClient: HttpClient) { }
  /*Get  Exchange*/ 
  getExchange(): Observable<Dbhelper>{
     return this.httpClient.post<Dbhelper>("http://clickurtrip.net/Webservices/genral/ExchnageHandler.asmx/GetCurrency",
     {ParentID:this.Parent},
     {
       headers: new HttpHeaders({
         'Content-Type': 'application/json'
      })
     });
 }

 /*Get  Country*/ 
 getCountry(): Observable<any>{
  return this.httpClient.post<any>("http://clickurtrip.net/Webservices/genral/locationHandler.asmx/GetCountry",
  {},
  {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
   })
  });
}
}
