import { TestBed } from '@angular/core/testing';

import { GlobalDefaultService } from './global-variable.service';

describe('GlobalVariableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalDefaultService = TestBed.get(GlobalDefaultService);
    expect(service).toBeTruthy();
  });
});
