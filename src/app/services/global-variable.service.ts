import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalDefaultService {
  Id: number;
  /*URL:string = "http://clickurtrip.net/Webservices/";http://localhost:50079*/
  URL:string;
  paramToBeUserInMultipleComponent: string;
  constructor() { 
    this.Id = 10;
    this.URL  = "http://localhost:50079/Webservices/";
    this.paramToBeUserInMultipleComponent = "Default Value";
  }
}
export class SerchParams_Tour{
  LocationID: string = "";;
  Date:string= "20-05-2019";
  TourType:string ="";
  constructor(){
    this.LocationID = "";
    this.Date = "20-05-2019";
    this.TourType ="";
  }
}
