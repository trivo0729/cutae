import { Component, OnInit } from '@angular/core';
import { ToursService } from 'src/app/services/tours.service';
import { FormControl ,ReactiveFormsModule, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { arrLocation, State, TourTypes, Search_Params } from 'src/app/modals/tours.modals';
import { Router } from '@angular/router';
import {map, startWith} from 'rxjs/operators';
import { DateFormat } from 'src/app/modals/date-format.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  stateCtrl = new FormControl();
  filteredStates: Observable<arrLocation[]>;
  Locations: arrLocation[] ;
  Adults:number=1; 
  Childs:number=0;
  Date:string="";
  value:string="" ;
  TourTypes:TourTypes[];
  // TourTypes:TourTypes[] =   [
  //   {Icon:"../../../assets/img/icons_search/all_tours.png",Name:"All tours",ActivityCount:0},
  //   {Icon:"../../../assets/img/icons_search/sightseeing.png",Name:"City sightseeing",ActivityCount:0},
  //   {Icon:"../../../assets/img/icons_search/museums.png",Name:"Museum tours",ActivityCount:0},
  //   {Icon:"../../../assets/img/icons_search/historic.png",Name:"Historic Buildings",ActivityCount:0}
  // ]
  constructor(private tourService: ToursService, private router: Router,
    ){
      this.Adults=1; 
      this.Childs=0;
    }
  ngOnInit() {
    this.tourService.getLocation('').subscribe((res: arrLocation[]) => {
      this.Locations = res;
      this.filteredStates = this.stateCtrl.valueChanges
      .pipe(
        startWith(''),
        map(Location => Location ? this._filterStates(Location) : this.Locations.slice())
      );
      console.log(res);
    });

    this.tourService.getAllTourtype().subscribe((res: TourTypes[]) => {
      this.TourTypes = res;
      console.log(res);
    });
    
  }
  myForm = new FormGroup({})
  Search_Params:Search_Params = new Search_Params();
  private _filterStates(value: string): arrLocation[] {
    const filterValue = value.toLowerCase();
    return this.Locations.filter(Location => Location.LocationName.toLowerCase().indexOf(filterValue) === 0);
  }
  
  SearchActivity(){
    debugger
    /*Set Date Format */
    if(this.Search_Params.sDate !="")
    {
      this.value ="";
      let addZeroToLoneFigure = (n) => n.toString().length === 1 ? '0' + n : n.toString();
      let d= new Date(this.Search_Params.sDate);
      this.value += addZeroToLoneFigure(d.getDate()) + "-";
      this.value +=addZeroToLoneFigure(d.getMonth() + 1) + "-";
      this.value +=addZeroToLoneFigure(d.getFullYear()) ;
      this.Search_Params.sDate =this.value;
    }
    /* LocationID */
    if(this.Locations != undefined && this.Search_Params.LocationID != undefined && this.Search_Params.LocationID =="")
    {
      this.Locations.forEach(Location => {
        if(Location.LocationName==this.Search_Params.LocationID)
        {
          this.Search_Params.LocationID = Location.Lid.toString();
        }
      });
    }
    else
       this.Search_Params.LocationID ="";
    this.tourService.Search_Params = this.Search_Params
    this.router.navigateByUrl('/tours-list');
  }

  AddPax(Type:string){
    debugger
  if(Type =="Adults")
  {
    this.Search_Params.Adults += this.Search_Params.Adults+1;
  }
  else
  {
    this.Search_Params.Child += this.Search_Params.Child+1;
  }
  }
}
