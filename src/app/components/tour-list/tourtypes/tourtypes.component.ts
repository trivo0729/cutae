import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Tours, TourType, DateRange, Slots, PaxTyp, TicketType, Booking_Tour } from 'src/app/modals/tours.modals';
import { ToursService } from 'src/app/services/tours.service';
import { GlobalDefaultService } from 'src/app/services/global-variable.service';


@Component({
  selector: 'app-tourtypes',
  templateUrl: './tourtypes.component.html',
  styleUrls: ['./tourtypes.component.css']
})
export class TourtypesComponent implements OnInit {
  
  constructor(
    private tourService: ToursService,
    private router:Router
  )
   {}
   ngOnInit() {
   }
   
}
