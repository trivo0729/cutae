import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourtypesComponent } from './tourtypes.component';

describe('TourtypesComponent', () => {
  let component: TourtypesComponent;
  let fixture: ComponentFixture<TourtypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourtypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourtypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
