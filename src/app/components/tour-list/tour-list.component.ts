import { Component, OnInit } from '@angular/core';
import { ToursService } from 'src/app/services/tours.service';
import { Tours, TourType, DateRange, Slots, TicketType, PaxTyp, Booking_Tour, resultTours, TourTypes, sPriceRange  } from 'src/app/modals/tours.modals';
import { Router } from '@angular/router';
import { GlobalDefaultService, SerchParams_Tour } from 'src/app/services/global-variable.service';
@Component({
  selector: 'app-tour-list',
  templateUrl: './tour-list.component.html',
  styleUrls: ['./tour-list.component.css']
})
export class TourListComponent implements OnInit {

  constructor(private tourService: ToursService, private router: Router,
  ){}
  arrResult : resultTours = new resultTours();
  price:sPriceRange = new sPriceRange();
  tours: Tours[] = [];
  arrDates:DateRange[]=[];
  arrSlots: Slots[]=[];
  arrTicketType : TicketType[]=[];
  objGlobal:GlobalDefaultService ;
  arrPax: PaxTyp[]=[];
  /* Show Hide Wizard*/
  selRateType:boolean = true;
  selDate:boolean = false;
  selSlot:boolean = false;
  selBook:boolean = false;
  showRate:boolean= false;

  ID:Number;
  Name:string;
  Location:string="";
  TourType:string="";
  RateType:string;
  Date:string ="20-04-2019";
  SlotDate: string;
  SlotName:string;
  tourType:TourType[] = [];
  arrTourType:TourTypes[]=[];
  ngOnInit() {
    this.tourService.getTours().subscribe((res: resultTours) => {
      console.log(res);
      this.arrResult = res;
      if( this.arrResult.retCode ==1)
      {
        this.tours =  this.arrResult.Activities;
        this.arrTourType =  this.arrResult.arrTouType;
        this.price = this.arrResult.PriceRange;
      }
      else
      {

      }
      console.log(res);
    });
  }
  getTourType(ID:Number,name:string) {
    this.setWizard("Type");
    this.ID = ID;
    this.Name=name;
    this.tourService.getTourtype(ID,this.Date).subscribe(
      (res: TourType[]) => {
      this.tourType = res;
      console.log(res);
    });
  }

  getRateDate(Type:string){
    this.RateType = Type;
    this.tourService.getDateRange({ Ratetype:this.RateType, ID: this.ID, Date: this.Date }).subscribe(
      (res: DateRange[]) => {
        this.setWizard("Date");
        this.arrDates = res;
      console.log(res);
    });
  }

  getSlot(Date:string){
    this.SlotDate=Date;
    this.tourService.getSlots({ Type: this.RateType, ID: this.ID, Date: Date}).subscribe(
      (res: Slots[]) => {
        this.setWizard("Slot");
        this.arrSlots = res;
      console.log(res);
    });
  }

  getSlotRate(SlotID:number,slotName:string){
    this.SlotName = slotName
    this.tourService.getSlotRate({ Type: this.RateType, ID: this.ID,SlotID:SlotID, Date: this.Date }).subscribe(
      (res: TicketType[]) => {
        this.showRate = true;
        this.arrTicketType = res;
        this.arrTicketType.forEach(arrTicket => {
          arrTicket.arrayPax.forEach(Pax => {
            this.arrPax.push(Pax);
          });
        });
      console.log(this.arrPax);
    });
  }
  setTicketRate(Ticket:string){
    this.setWizard("Book")
  }
  setWizard(Type:string):void{
      switch(Type) 
      {
        case "Type":
        this.selDate = false;
        this.selSlot = false;
        this.selBook = false;
        this.selRateType = true;
        break;
        case "Date":
        this.selDate = true;
        this.selSlot = false;
        this.selBook = false;
        this.selRateType = false;
        break;
        case "Slot":
        this.selDate = false;
        this.selSlot = true;
        this.selBook = false;
        this.selRateType = false;
        break;
        case "Book":
        this.selDate = false;
        this.selSlot = false;
        this.selBook = true;
        this.selRateType = false;
        break;
      }
  }

  /*Add To Cart */
  Booking_Items:Booking_Tour[];
  Tour:Booking_Tour = new Booking_Tour();


  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }
    if (value >= 1000) {
      return value + 'k';
    }
    return value;
  }
}
