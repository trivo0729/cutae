import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourfilterComponent } from './tourfilter.component';

describe('TourfilterComponent', () => {
  let component: TourfilterComponent;
  let fixture: ComponentFixture<TourfilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourfilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourfilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
