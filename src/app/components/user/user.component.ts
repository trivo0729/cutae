import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User,loginDetails } from 'src/app/modals/user.model';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [UserService]
})
export class UserComponent implements OnInit {
  constructor(private userService:UserService, private router: Router,public dialog: MatDialog,
    public dialogRef: MatDialogRef<UserComponent>) { }
 
  public login:loginDetails;
  Response:any;
  sUserName:string;
  sPassword:string;
  sParentID:number=232;
  UserDetails:User = new User();

  ngOnInit() {
    this.login= this.userService.getLogedIn();
    this.UserDetails =  this.login.LoginDetail;
  }
  getUserlogin(){
    debugger
    this.userService.UserLogin(this.sUserName,this.sPassword,this.sParentID).subscribe(
      (res: loginDetails) => {
       this.userService.setLogedIn(JSON.stringify(res));
       this.login = res;
       if(res.retCode ==1)
       {
        this.UserDetails = res.LoginDetail;
        this.router.navigateByUrl('/profile');
        this.dialogRef.close();
       }
    });
  }
}
