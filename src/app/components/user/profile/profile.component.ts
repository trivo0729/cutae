import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { loginDetails, User } from 'src/app/modals/user.model';
import { GenralService } from 'src/app/services/genral.service';
import { Console } from '@angular/core/src/console';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private userService:UserService,private genralService :GenralService) { }
  public arrDetails : loginDetails = new loginDetails();
  public arrCountry : any[];
  public result:any;
  public LoginDetails: User = new User();

  ngOnInit() {
    debugger
   this.arrDetails= this.userService.getLogedIn();
   this.LoginDetails = this.arrDetails.LoginDetail;
   this.getCountry();
  }

  getCountry()
  {
    this.genralService.getCountry().subscribe(
      (res: any) => {
     this.arrCountry = res;
    });
  }
}
