import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TourListComponent } from './components/tour-list/tour-list.component';
import { HomeComponent } from './components/home/home.component';
import { SignInComponent } from "./components/sign-in/SignInComponent";
import { ProfileComponent } from './components/user/profile/profile.component';
import { BookingsComponent } from './components/user/bookings/bookings.component';
import { WishlistComponent } from './components/user/wishlist/wishlist.component';
import { ChangepasswordComponent } from './components/user/changepassword/changepassword.component';

const routes: Routes = [
  {path: 'tours-list', component: TourListComponent},
  {path: '',component: HomeComponent},
  {path: 'home',component: HomeComponent},
  {path: 'profile',component: ProfileComponent},
  {path: 'bookings',component: BookingsComponent},
  {path: 'Wishlist',component: WishlistComponent},
  {path: 'changepassword',component: ChangepasswordComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
