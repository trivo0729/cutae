import { UserComponent } from '../components/user/user.component';
import { MatDialogRef, MatDialog } from '@angular/material';

export  class Poups {
    constructor(public dialog: MatDialog,public dialogRef: MatDialogRef<UserComponent>){}
    openDialog() {
        this.dialogRef = this.dialog.open(UserComponent);
        this.dialogRef.afterClosed().subscribe(result => {
       
        });
      }
      closeDialog() {
        this.dialogRef.close('Pizza!');
      }
}

