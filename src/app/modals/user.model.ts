export class User {
    sid: number ;
    Name :string ;
    Email: string  ;
    ContactID: number;
    ParentId:number ;
    Password: string ;
    Remarks: string ;
    ContacDetails:ContacDetails
}
export class  ContacDetails {
    Mobile: string;
    Phone: string ;
    CitiesID: string ;
    CountryID: string ;
    Address: string ;
}

export class loginDetails{
    LoginDetail:User;
    retCode=0;
    Message=1;
}