import { Dbhelper } from './dbhelper.model';
import { ToursService } from '../services/tours.service';

export class Tours { 
ActivityID: number;
Supplier: string;
Name:string;
City:string;
Country:string;
CheckIN:string;
Description:string;
Tour_Note:string;
List_Mode: any;
List_Type:any;
Duration:string;
TotalPrice:number;
CurrencyClass:string;
ListImage: Image;
Tooltip:string;
Opening:string;
Closing:string;
}
export class Image{
    Count :string;
    IsDefault :string;
    Title :string;
    Type :string;
    Url :string;
}

export class TourType { 
    Type:string;
}
export class DateRange{
    sDate:string;
    fDate:string;
    Month:string;
    Day:string;
    Date:string;
    Available:boolean
}
export class Slots{
    ID: number;
    SlotsStartTime:string;
    SlotsEndTime:string;
}
export class TicketType{
    Ticket:string;
    TicketID:string;
    arrayPax:PaxTyp[];
}
export class PaxTyp{
    Pax_Type:string;
    Rate:string;
    Age:string;        
}

export class arrLocation{
    LocationName:string;
    City:string;
    Lid:number;
    Country:string;
    Latitude:string;
    Longitutde:string;
}

export class State{
    LocationName:string;
    City:string;
    flag:string;
}

export class TourTypes{
    Icon:string;
    Name:string;
    ActivityCount:number
}

export class Booking_Tour{
Sr_No:number;
Request_Id:number;
Activity_Id:number;
Passenger_Name:string;
Email:string;
Contact_No:string;
Adults:number;
Child1:number;
Child2:number;
Infant:number;
Adult_Cost:number;
Child1_Cost:number;
Child2_Cost:number;
Infant_Cost:number;
Extras_Addon:number;
Taxes:number;
Discount:number;
TotalAmount:number;
User_Id:string;
IsPaid:boolean;
PaymentTransaction_Id:string;
Pickup_Location:string;
Drop_Location:string;
Comment:string;
Vehicle_Id:number;
Driver_Id:number;
Booking_Date :string;
Invoice_No :string;
 Voucher_No :string;
ParentID:number;
Sightseeing_Date:string;
Status:string;
SlotID :number;
Ticket_Type:number;
Activity_Type:string;
BookingType:string;
InventoryType:string;
}

export class Search_Params{
    LocationID:string;
    TourType:string;
    sDate:string;
    Time:string
    Adults:number;
    Child:number;
    constructor(){
        this.LocationID="";
        this.TourType="";
        this.sDate="";
        this.Time="12:00";
        this.Adults=1;
        this.Child=0;
    }

}

/*Search Resuls */

export class resultTours{
    
    retCode:number;
    Activities:Tours[];
    arrTouType:TourTypes[];
    PriceRange:sPriceRange;
    Message:string
}

export class sPriceRange{
    MinPrice:number;
    MaxPrice:number
}

export class Rate_Params{
  ID:Number=0;
  Name:string;
  Location:string;
  TourType:string;
  RateType:string;
  Date:string;
  SlotDate: string;
  SlotName:string;
  tourType:TourType[] = [];
  constructor(private tourService: ToursService){}
}