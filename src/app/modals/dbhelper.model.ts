export class Dbhelper {
    retCode:number;
    Message:string;
    arrCurrency:any
}
export class ExchangeRate
{
    BaseCurrency: string ;
    Currency: string  ;
    Exchange: number;
    LastUpdateDt: string ;
    MailFlag: boolean;
    Rate: number ;
}