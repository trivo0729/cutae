import { Component } from '@angular/core';
import { from } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material';
import { UserComponent } from './components/user/user.component';
import { GenralService } from './services/genral.service';
import { ExchangeRate, Dbhelper } from './modals/dbhelper.model';
import { loginDetails } from './modals/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [GenralService, ExchangeRate,Dbhelper ]

})
export class AppComponent {
  title = 'CutAe';
  public Currency:string ="AED";
  
  constructor(public dialog: MatDialog, 
    public genralService:GenralService){}
  openDialog() {
    const dialogRef = this.dialog.open(UserComponent);
    dialogRef.afterClosed().subscribe(result => {
     this.login.retCode =1;
    });
  }
  public exchange: ExchangeRate[];
  public login:loginDetails = new loginDetails();
  ngOnInit() {
    debugger
   this.genralService.getExchange().subscribe((res: Dbhelper) => {
     console.log(res);
     if( res.retCode ==1)
     {
        this.exchange =  res.arrCurrency;
     }
   });
  }
  SetCurrency(sCurrency:string)
  {
    this.Currency = sCurrency
  }
}


