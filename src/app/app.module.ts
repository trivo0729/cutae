import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TourListComponent } from './components/tour-list/tour-list.component';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import { TourtypesComponent } from './components/tour-list/tourtypes/tourtypes.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { MatAutocompleteModule, MatFormFieldModule ,MatInputModule,MatDatepickerModule,
   MatNativeDateModule, MatSelect, MatSelectModule, MAT_DATE_FORMATS, 
   NativeDateModule,MatRippleModule, MatIconModule, 
   MAT_DATE_LOCALE, DateAdapter, MatSliderModule, MatDialogModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserComponent } from './components/user/user.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { BookingsComponent } from './components/user/bookings/bookings.component';
import { ChangepasswordComponent } from './components/user/changepassword/changepassword.component';
import { WishlistComponent } from './components/user/wishlist/wishlist.component';
import { EditprofileComponent } from './components/user/editprofile/editprofile.component'; 




@NgModule({
  declarations: [
    AppComponent,
    TourListComponent,
    HomeComponent,
    TourtypesComponent,
    SignInComponent,
    UserComponent,
    ProfileComponent,
    BookingsComponent,
    ChangepasswordComponent,
    WishlistComponent,
    EditprofileComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule, 
    MatSelectModule,
    MatFormFieldModule,
    MatSliderModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatRippleModule ,
    HttpClientModule,
    MatDatepickerModule,        // <----- import(must)
    NativeDateModule,
    MatNativeDateModule,        // <----- import for date formating(optional)
    MatFormFieldModule,
    MatInputModule,
    FormsModule ,
    MatIconModule,
    MatDialogModule,
  ],
  bootstrap: [AppComponent],
  entryComponents:[UserComponent],
  providers: [
  
  ],
})
export class AppModule { }

